---
title: Dual Boot Kali with Windows
description:
icon:
date: 2020-05-28
type: archived
weight: 20
author: ["g0tmi1k",]
tags: ["",]
keywords: ["",]
og_description:
---

This has been moved to the [installation](https://www.kali.org/docs/installation/) section. To access this doc specifically, please follow [this link](https://www.kali.org/docs/installation/kali-linux-dual-boot-kali-with-windows/).